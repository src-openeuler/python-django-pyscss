%global _empty_manifest_terminate_build 0
Name:		python-django-pyscss
Version:	2.0.3
Release:	1
Summary:	Makes it easier to use PySCSS in Django.
License:	BSD-2-Clause
URL:		https://github.com/fusionbox/django-pyscss
Source0:	https://files.pythonhosted.org/packages/fc/71/75737fc65a3563ead1fb60ccad5459cf2342f2fc009006294e8f78d04c1e/django-pyscss-2.0.3.tar.gz	
BuildArch:	noarch


%description
A collection of tools for making it easier to use pyScss within Django.
    This version only supports pyScss 1.3.4 and greater. For pyScss 1.2 support,
    you can use the 1.x series of django-pyscss.

%package -n python3-django-pyscss
Summary:	Makes it easier to use PySCSS in Django.
Provides:	python-django-pyscss
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-django-pyscss
A collection of tools for making it easier to use pyScss within Django.
    This version only supports pyScss 1.3.4 and greater. For pyScss 1.2 support,
    you can use the 1.x series of django-pyscss.

%package help
Summary:	Development documents and examples for django-pyscss
Provides:	python3-django-pyscss-doc
%description help
A collection of tools for making it easier to use pyScss within Django.
    This version only supports pyScss 1.3.4 and greater. For pyScss 1.2 support,
    you can use the 1.x series of django-pyscss.

%prep
%autosetup -n django-pyscss-2.0.3

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-django-pyscss -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Dec 01 2023 zhangkea <zhangkea@uniontech.com> - 2.0.3-1
- update version 2.0.3
  Support for Django 4.2. Drop support for Python 2

* Mon May 09 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 2.0.2-2
- License compliance rectification

* Fri Jan 29 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
